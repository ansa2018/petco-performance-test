package com.yantriks.perf.petco.jmeter.helper;

import org.apache.log.Logger;

/**
 * Created by berniewong on 5/9/17.
 */
public class Log {

    Logger logger = null;

    public Log(Logger log) {
        this.logger = log;
    }

    public void info(String msg) {
        if (logger == null) {
            System.out.println("INFO:" + msg);
        } else {
            logger.info(msg);
        }
    }

    public void error(String msg) {
        if (logger == null) {
            System.out.println("ERROR:" + msg);
        } else {
            logger.error(msg);
        }
    }
}
