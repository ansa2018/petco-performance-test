package com.yantriks.perf.petco.jmeter.helper;

/**
 * Created by berniewong on 5/11/17.
 */
public class SFSConst {
//    JmeterVariables Keys
    public static final String VK_CONTROLLER_NAME = "controllerName";
    public static final String VK_REQUEST_NAME = "requestName";
    public static final String VK_RESPONSE_JSON = "responseJson";
    public static final String VV_PERFORM_PACK_CONTROLLER = "perform_pack_controller";
}