package com.yantriks.perf.petco.jmeter.helper;

import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import org.apache.jmeter.threads.JMeterVariables;
import org.apache.logging.slf4j.Log4jLogger;


import java.util.HashMap;
import java.util.Map;

/**
 * Created by berniewong on 5/10/17.
 */
public class PickHelperForLogin {

    public String processLogin(JMeterVariables vars, Log4jLogger log, JsonObject jsonObject) {

        int pickCount = 0;
        JsonArray pickShipmentArray = (jsonObject.get("controllerData").asObject().get("wsc_components_batch_batchlist_BatchPickListInitController").asObject().get("batchPick_getBatchListForStore").asObject().get("Page").asObject().get("Output").asObject().get("StoreBatchList").asObject().get("StoreBatch")).asArray();

        JsonObject lastRecordOnPage = jsonObject.get("controllerData").asObject().get("wsc_components_batch_batchlist_BatchPickListInitController").asObject().get("batchPick_getBatchListForStore").asObject().get("Page").asObject().get("LastRecord").asObject();
        String isLastPage = jsonObject.get("controllerData").asObject().get("wsc_components_batch_batchlist_BatchPickListInitController").asObject().get("batchPick_getBatchListForStore").asObject().get("Page").asObject().get("IsLastPage").asString();
        //last record on page details - woudl be requied if next page is called


        //Iterate through array to get a shipment not assigned to another user

        String assignedToUserID = null;
        String storeBatchKey = null;
        String newRequest = null;
        String currentOrderNo = null;
        String pickStatus = null;
        String username = vars.get("username");

        // TODO - review
        Map mapFailedOrderNumbers = (Map) (vars.getObject("failedOrderNumbers"));
        if (mapFailedOrderNumbers == null) {
            log.info(String.format("creating failedOrderNumbers map for %s -- which should only happen once per user", username));
            mapFailedOrderNumbers = new HashMap();
            vars.putObject("failedOrderNumbers", mapFailedOrderNumbers);
        }

        String batchNo = null;
        String storeBatchConfigList = null;
        String imageAltText = null;
        String imageUrl = null;
        String oldestExpShpDate = null;
        JsonObject pickRequest = null;
        String pickBatchDetails = null;
        String pickOneRequest = null;
        String controllerName = null;

        while (pickCount < pickShipmentArray.size()) {
            log.info("in while");
            if (pickShipmentArray.get(pickCount).asObject().get("AssignedToUsername") != null) {
                assignedToUserID = pickShipmentArray.get(pickCount).asObject().get("AssignedToUsername").asString();
                storeBatchKey = (pickShipmentArray.get(pickCount).asObject().get("StoreBatchKey")).asString();
                newRequest = "N";
                log.info("in if");
            } else {
                assignedToUserID = vars.get("username");
                if (pickShipmentArray.get(pickCount).asObject().get("StoreBatchKey") != null) {
                    storeBatchKey = (pickShipmentArray.get(pickCount).asObject().get("StoreBatchKey")).asString();
                    newRequest = "N";
                    currentOrderNo = (pickShipmentArray.get(pickCount).asObject().get("BatchNo")).asString();
                } else {
                    storeBatchKey = null;
                    newRequest = "Y";
                    currentOrderNo = "";
                }
            }

            pickStatus = pickShipmentArray.get(pickCount).asObject().get("Status").asString();
            log.info("after pick status");
            log.info("after last order map" + currentOrderNo + "last order map" + mapFailedOrderNumbers);
            if ((currentOrderNo != "" && mapFailedOrderNumbers.get(currentOrderNo) == null) && (pickStatus.equals("1000") || pickStatus.equals("1100")) && (assignedToUserID.equals(vars.get("username")))) {
                log.info("in the if loop");
                batchNo = (pickShipmentArray.get(pickCount).asObject().get("BatchNo")).asString();
                storeBatchConfigList = pickShipmentArray.get(pickCount).asObject().get("StoreBatchConfigList").toString();
                imageAltText = pickShipmentArray.get(pickCount).asObject().get("ImageAltText").asString();
                imageUrl = pickShipmentArray.get(pickCount).asObject().get("ImageUrl").asString();
                oldestExpShpDate = pickShipmentArray.get(pickCount).asObject().get("OldestExpShpDate").asString();
                pickRequest = pickShipmentArray.get(pickCount).asObject();
                pickBatchDetails = pickRequest.get("ShipmentLines").toString();

                //this forces the loop to exit
                pickCount = pickShipmentArray.size();
            }

            log.info("after last");
            vars.putObject("failedOrderNumbers", mapFailedOrderNumbers);
            pickCount++;
            //store last record in case we need to navigate to next page
        }

        if (pickRequest != null) {
	            /*	pickOneRequest = "{\"scWizardDefId\":\"extn.components.batch.batchpick.sortwhilepick.SortWhilePickupWizardExtn\",\"isWizard\":true,\"scControllerInput\":{\"StoreBatch\":{\"Status\":\""+pickStatus+"\",\"StoreBatchKey\":\""+storeBatchKey.trim()+"\",\"BatchNo\":\""+batchNo+"\",\"ShipmentLines\":"+pickBatchDetails +"},"+"\"StoreBatchConfigList\":"+ storeBatchConfigList.toString() +",\"BatchType\":\""+batchType+"\",\"ImageAltText\":\""+imageAltText+"\",\"ImageUrl\":\""+imageUrl+"\",\"OldestExpShpDate\":\""+oldestExpShpDate+"\"}}}";*/
            if (newRequest.equals("N")) {
                pickOneRequest = "{\"scWizardDefId\":\"extn.components.batch.batchpick.sortwhilepick.SortWhilePickupWizardExtn\",\"isWizard\":true,\"scControllerInput\":{\"StoreBatch\":{\"Status\":\"" + pickStatus + "\",\"BatchNo\":\"" + batchNo + "\", \"BatchType\": \"SORT_AFTER_PICK\",\"ShipmentLines\":" + pickBatchDetails + ",\"StoreBatchConfigList\":" + storeBatchConfigList + ",\"StoreBatchKey\": \"" + storeBatchKey + "\", \"ImageAltText\":\"" + imageAltText + "\",\"OldestExpShpDate\":\"" + oldestExpShpDate + "\"}}}}";
            } else {
                pickOneRequest = "{\"scWizardDefId\":\"extn.components.batch.batchpick.sortwhilepick.SortWhilePickupWizardExtn\",\"isWizard\":true,\"scControllerInput\":{\"StoreBatch\":{\"Status\":\"" + pickStatus + "\",\"BatchNo\":" + batchNo + ",\"ShipmentLines\":" + pickBatchDetails + ",\"StoreBatchConfigList\":" + storeBatchConfigList + ",\"ImageAltText\":\"" + imageAltText + "\",\"OldestExpShpDate\":\"" + oldestExpShpDate + "\"}}}}";
            }
            controllerName = "pick_controller";

        } else {
            if (isLastPage.equals("Y")) {
                int exitCounter = 9999;
                vars.put("exitCounter", "" + exitCounter);
                controllerName = "none";
            } else {
//                batchNo = lastRecordOnPage.get("StoreBatch").asObject().get("BatchNo").asString();
//                pickStatus = lastRecordOnPage.get("StoreBatch").asObject().get("Status").asString();
                controllerName = "next_page_controller";
            }
        }

        vars.put("lastRecordOnPage", "" + lastRecordOnPage);
        vars.put("pickOneRequest", "" + pickOneRequest);
        vars.put("controllerName", controllerName);
        return "OK";
    }
}
