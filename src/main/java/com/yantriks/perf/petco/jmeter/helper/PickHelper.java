package com.yantriks.perf.petco.jmeter.helper;

import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import org.apache.jmeter.threads.JMeterVariables;
import org.apache.logging.slf4j.Log4jLogger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by berniewong on 5/9/17.
 */


public class PickHelper {

    public void sayHello( Log4jLogger log) {
        log.info("PickHelper v0.11");

    }

    public String processPickResponse(JMeterVariables vars, Log4jLogger log) {


//Store response in a variable
        String isJunkOrder = "N";
        int pickCount = 0;
        JsonObject pickRequest = null;
        String assignedToUserID = null;
        JsonObject lastRecordOnPage = null;
        String pickStatus = null;
        JsonArray pickShipmentArray = null;
        String pickOneRequest = null;
        String pickBatchDetails = null;
        String storeBatchKey = null;
        String batchNo = null;
        String storeBatchConfigList = null;
        String batchType = null;
        String imageAltText = null;
        String imageUrl = null;
        String oldestExpShpDate = null;
        String StoreBatchKey = null;
        String itemID = null;
        String backroomPickedQuantity = null;
        String totalQuantity = null;
        String totalNumberOfRecords = null;
        String responseJson = vars.get("responseJson");
        String requestName = vars.get("requestName");
        String isLastPage = null;
        ArrayList pickList = new ArrayList();
        Map itemInfoMap = null;
        Map batchPickMap = new HashMap();
        String newRequest = null;
        Map lastOrderNumberMap = new HashMap();
        String currentOrderNo = "";
        String controllerName = "invalid";

        JsonObject jsonObject = JsonObject.readFrom(responseJson);
        //Divya - Commented this
        //vars.remove("responseJson");
        log.info("the picklist on getting back is " + vars.getObject("pickList"));


        if (requestName.equals("LOGIN-ALTERNATE")) {
            vars.put("controllerName", "filter_page_controller");
            return "OK";
        }

        if (requestName.equals("ERROR")) {
            vars.put("controllerName", "dump_the_world");
            return "OK";
        }

        if (requestName.equals("INCREASEQTY")) {
            //Divya - Added to reset the batchpickMap and picklist
            vars.putObject("batchPickMap",null);
            vars.putObject("pickList",null);

            vars.put("controllerName", "finish_pick_controller");
            return "OK";
        }

        if (requestName.equals("NEXTPRODPAGE")) {
            vars.put("controllerName", "next_prod_page_controller");
            return "OK";
        }

        if (requestName.equals("FINISH") || requestName.equals("LOGIN_TOKEN")) {
            vars.put("controllerName", "home_controller");
            return "OK";
        }

        if (requestName.equals("LOGIN")||(requestName.equals("NEXTPAGE"))) {
            log.info("$$$$$$ inside login");
            System.out.println(">>>>>> inside login");
            return processLogin(vars, log, jsonObject);
        }
        if (requestName.equals("VALIDATEJSON")) {
            return isValidJsonResponse(vars, log, jsonObject);
        }

        if (requestName.equals("FILTERPAGE")) {

            //Create an array of all the picks displayed on the page
            pickShipmentArray = (jsonObject.get("controllerData").asObject().get("wsc_components_batch_batchlist_BatchPickListBehaviorController").asObject().get("batchPick_getBatchListForStore_behavior").asObject().get("Page").asObject().get("Output").asObject().get("StoreBatchList").asObject().get("StoreBatch")).asArray();

            lastRecordOnPage = jsonObject.get("controllerData").asObject().get("wsc_components_batch_batchlist_BatchPickListBehaviorController").asObject().get("batchPick_getBatchListForStore_behavior").asObject().get("Page").asObject().get("LastRecord").asObject();
            isLastPage = jsonObject.get("controllerData").asObject().get("wsc_components_batch_batchlist_BatchPickListBehaviorController").asObject().get("batchPick_getBatchListForStore_behavior").asObject().get("Page").asObject().get("IsLastPage").asString();
            //last record on page details - woudl be requied if next page is called
            //storeBatchKey = (lastRecordOnPage.get(pickCount).get("StoreBatchKey")).asString();

            //Iterate through array to get a shipment not assigned to another user

            while (pickCount < pickShipmentArray.size()) {
                totalNumberOfRecords = (jsonObject.get("controllerData").asObject().get("wsc_components_batch_batchlist_BatchPickListBehaviorController").asObject().get("batchPick_getBatchListForStore_behavior").asObject().get("Page").asObject().get("Output").asObject().get("StoreBatchList").asObject().get("TotalNumberOfRecords")).asString();

                //Read status of each shipment and pick the one that has status "Ready To Pick";
                //the item could be in progress or a new batch and based on that there would or would not be a value for assignedtouserid
                if (pickShipmentArray.get(pickCount).asObject().get("AssignedToUsername") != null) {
                    assignedToUserID = pickShipmentArray.get(pickCount).asObject().get("AssignedToUsername").asString();
                } else {
                    assignedToUserID = vars.get("username");
                }

                pickStatus = pickShipmentArray.get(pickCount).asObject().get("Status").asString();

                if ((pickStatus.equals("1000") || pickStatus.equals("1100")) && (assignedToUserID.equals(vars.get("username")))) {
                    batchNo = (pickShipmentArray.get(pickCount).asObject().get("BatchNo")).asString();
                    storeBatchConfigList = pickShipmentArray.get(pickCount).asObject().get("StoreBatchConfigList").toString();
                    // batchType = (pickShipmentArray.get(pickCount).get("BatchType")).asString();
                    imageAltText = pickShipmentArray.get(pickCount).asObject().get("ImageAltText").asString();
                    imageUrl = pickShipmentArray.get(pickCount).asObject().get("ImageUrl").asString();
                    oldestExpShpDate = pickShipmentArray.get(pickCount).asObject().get("OldestExpShpDate").asString();
                    pickRequest = pickShipmentArray.get(pickCount).asObject();
                    pickBatchDetails = pickRequest.get("ShipmentLines").toString();
                    //this forces the loop to exit
                    pickCount = pickShipmentArray.size();

                }

                pickCount++;
                //store last record in case we need to navigate to next page
            }
            if (pickRequest != null) {

	/*	pickOneRequest = "{\"scWizardDefId\":\"extn.components.batch.batchpick.sortwhilepick.SortWhilePickupWizardExtn\",\"isWizard\":true,\"scControllerInput\":{\"StoreBatch\":{\"Status\":\""+pickStatus+"\",\"StoreBatchKey\":\""+storeBatchKey.trim()+"\",\"BatchNo\":\""+batchNo+"\",\"ShipmentLines\":"+pickBatchDetails +"},"+"\"StoreBatchConfigList\":"+ storeBatchConfigList.toString() +",\"BatchType\":\""+batchType+"\",\"ImageAltText\":\""+imageAltText+"\",\"ImageUrl\":\""+imageUrl+"\",\"OldestExpShpDate\":\""+oldestExpShpDate+"\"}}}";*/
                pickOneRequest = "{\"scWizardDefId\":\"extn.components.batch.batchpick.sortwhilepick.SortWhilePickupWizardExtn\",\"isWizard\":true,\"scControllerInput\":{\"StoreBatch\":{\"Status\":\"" + pickStatus + "\",\"BatchNo\":" + batchNo + ",\"ShipmentLines\":" + pickBatchDetails + ",\"StoreBatchConfigList\":" + storeBatchConfigList + ",\"ImageAltText\":\"" + imageAltText + "\",\"OldestExpShpDate\":\"" + oldestExpShpDate + "\"}}}}";
                controllerName = "pick_controller";
            } else {
                if (isLastPage.equals("Y")) {
                    int exitCounter = 9999;
                    vars.put("exitCounter", "" + exitCounter);
                    controllerName = "none";
                } else {
                    batchNo = lastRecordOnPage.get("StoreBatch").asObject().get("BatchNo").asString();
                    pickStatus = lastRecordOnPage.get("StoreBatch").asObject().get("Status").asString();
                    controllerName = "next_page_controller";
                }
            }

            vars.put("lastRecordOnPage", "" + lastRecordOnPage);
            vars.put("pickOneRequest", "" + pickOneRequest);
            vars.put("controllerName", controllerName);
            return "OK";
        }

        if (requestName.equals("PICK")) {
            JsonObject pickShipmentNode = null;
            if (jsonObject.get("controllerData").asObject().get("wsc_components_batch_batchpick_sortafterpick_AssignStagingLocationBatchLineListInitController") != null) {
                if(jsonObject.get("controllerData").asObject().get("wsc_components_batch_batchpick_sortafterpick_AssignStagingLocationBatchLineListInitController").asObject().get("getStoreBatchLinesListInit").asObject().get("Page").asObject().get("Output").asObject().get("StoreBatch").asObject().get("Status").asString().equals("2000")){
                    pickShipmentNode = jsonObject.get("controllerData").asObject().get("wsc_components_batch_batchpick_sortafterpick_AssignStagingLocationBatchLineListInitController").asObject().get("getStoreBatchLinesListInit").asObject().get("Page").asObject().get("Output").asObject().get("StoreBatch").asObject();

                    storeBatchKey = (pickShipmentNode.get("StoreBatchKey")).asString();
                    batchNo = (pickShipmentNode.get("BatchNo")).asString();
                    vars.put("batchNo", batchNo);
                    log.info("store batch key is " + storeBatchKey);

                    vars.put("storeBatchKey", storeBatchKey);
                    vars.put("controllerName", "finish_pick_controller");
                    return "OK";

                }
            }else if (jsonObject.get("controllerData").asObject().get("wsc_components_batch_batchpick_common_BatchPickupProductScanInitController") != null) {
                log.info("*********  batch list");

                pickShipmentNode = jsonObject.get("controllerData").asObject().get("wsc_components_batch_batchpick_common_BatchPickupProductScanInitController").asObject().get("getStoreBatchLinesListInit").asObject().get("Page").asObject().get("Output").asObject().get("StoreBatch").asObject();
                batchNo = (pickShipmentNode.get("BatchNo")).asString();
                if (!jsonObject.get("controllerData").asObject().get("wsc_components_batch_batchpick_common_BatchPickupProductScanInitController").asObject().get("getStoreBatchLinesListInit").asObject().get("Page").asObject().get("Output").asObject().get("StoreBatch").asObject().get("StoreBatchLines").asObject().get("TotalNumberOfRecords").asString().equals("0")) {
                    log.info("here in if %%%");
                    pickShipmentArray = (jsonObject.get("controllerData").asObject().get("wsc_components_batch_batchpick_common_BatchPickupProductScanInitController").asObject().get("getStoreBatchLinesListInit").asObject().get("Page").asObject().get("Output").asObject().get("StoreBatch").asObject().get("StoreBatchLines").asObject().get("StoreBatchLine")).asArray();
                } else {
                    isJunkOrder = "Y";
                    batchNo = (pickShipmentNode.get("BatchNo")).asString();
                    lastOrderNumberMap = (Map) vars.getObject("failedOrderNumbers");
                    if (lastOrderNumberMap == null) {

                        lastOrderNumberMap = new HashMap();
                        lastOrderNumberMap.put(batchNo, batchNo);


                    } else {
                        lastOrderNumberMap.put(batchNo, batchNo);
                    }

                    vars.put("controllerName", "dump_the_world");
                    vars.putObject("failedOrderNumbers", lastOrderNumberMap);
                    log.info("about to return:  " + lastOrderNumberMap.get(batchNo));
                    return "OK";
                }

                lastRecordOnPage = jsonObject.get("controllerData").asObject().get("wsc_components_batch_batchpick_common_BatchPickupProductScanInitController").asObject().get("getStoreBatchLinesListInit").asObject().get("Page").asObject().get("LastRecord").asObject();

                totalNumberOfRecords = "" + pickShipmentArray.size();
                isLastPage = jsonObject.get("controllerData").asObject().get("wsc_components_batch_batchpick_common_BatchPickupProductScanInitController").asObject().get("getStoreBatchLinesListInit").asObject().get("Page").asObject().get("IsLastPage").asString();
            } else if (jsonObject.get("controllerData").asObject().get("wsc_components_batch_batchlist_BatchPickListBehaviorController") != null) {
                pickShipmentNode = jsonObject.get("controllerData").asObject().get("wsc_components_batch_batchlist_BatchPickListBehaviorController").asObject().get("batchPick_getBatchListForStore_behavior").asObject().get("Page").asObject().get("Output").asObject().get("StoreBatch").asObject().asObject();
                pickShipmentArray = (jsonObject.get("controllerData").asObject().get("wsc_components_batch_batchlist_BatchPickListBehaviorController").asObject().get("batchPick_getBatchListForStore_behavior").asObject().get("Page").asObject().get("Output").asObject().get("StoreBatch").asObject().get("StoreBatchLines").asObject().get("StoreBatchLine")).asArray();

                lastRecordOnPage = jsonObject.get("controllerData").asObject().get("wsc_components_batch_batchlist_BatchPickListBehaviorController").asObject().get("batchPick_getBatchListForStore_behavior").asObject().get("Page").asObject().get("LastRecord").asObject();

                totalNumberOfRecords = "" + pickShipmentArray.size();
                isLastPage = jsonObject.asObject().get("controllerData").asObject().get("wsc_components_batch_batchlist_BatchPickListBehaviorController").asObject().get("batchPick_getBatchListForStore_behavior").asObject().get("Page").asObject().get("IsLastPage").asString();

            } else if (jsonObject.get("controllerData").asObject().get("wsc_components_batch_batchpick_common_BatchPickupProductScanBehaviorController") != null) {
                pickShipmentNode = jsonObject.get("controllerData").asObject().get("wsc_components_batch_batchpick_common_BatchPickupProductScanBehaviorController").asObject().get("getToBePickedStoreBatchLines").asObject().get("Page").asObject().get("Output").asObject().get("StoreBatch").asObject();
                pickShipmentArray = (jsonObject.get("controllerData").asObject().asObject().get("wsc_components_batch_batchpick_common_BatchPickupProductScanBehaviorController").asObject().get("getToBePickedStoreBatchLines").asObject().get("Page").asObject().get("Output").asObject().get("StoreBatch").asObject().get("StoreBatchLines").asObject().get("StoreBatchLine")).asArray();

                lastRecordOnPage = jsonObject.get("controllerData").asObject().get("wsc_components_batch_batchpick_common_BatchPickupProductScanBehaviorController").asObject().get("getToBePickedStoreBatchLines").asObject().get("Page").asObject().get("LastRecord").asObject();
                totalNumberOfRecords = "" + pickShipmentArray.size();
                isLastPage = jsonObject.get("controllerData").asObject().get("wsc_components_batch_batchpick_common_BatchPickupProductScanBehaviorController").asObject().get("getToBePickedStoreBatchLines").asObject().get("Page").asObject().get("IsLastPage").asString();
                log.info("here 5");
            }else{
                vars.put("controllerName", "dump_the_world");
                return "OK";

            }

            if (isJunkOrder.equals("N")) {
                if (vars.getObject("batchPickMap") != null) {
                    batchPickMap = (Map) vars.getObject("batchPickMap");
                    //Divya - Added to retain the pciklist
                    pickList = (ArrayList)vars.getObject("pickList");

                }
                pickStatus = pickShipmentNode.get("Status").asString();
                storeBatchKey = (pickShipmentNode.get("StoreBatchKey")).asString();
                batchNo = (pickShipmentNode.get("BatchNo")).asString();
                batchPickMap.put("batchNo", batchNo);
                batchPickMap.put("storeBatchKey", storeBatchKey);
                batchPickMap.put("pickStatus", pickStatus);
                //Iterate through array to get a shipment not assigned to another user
                while (pickCount < pickShipmentArray.size()) {
                    //Read status of each shipment and pick the one that has status "Ready To Pick";
                    pickRequest = pickShipmentArray.get(pickCount).asObject();
                    itemID = (pickRequest.get("ItemID")).asString();
                    //Divya - Added
                    totalQuantity = (pickRequest.get("Quantity")).asString();
                    backroomPickedQuantity = (pickRequest.get("BackroomPickedQuantity")).asString();
                    itemInfoMap = new HashMap();
                    itemInfoMap.put("itemID", itemID);

                    double qtyToPick = 0.0;
                    log.info("The picked quantity is " + backroomPickedQuantity + "total is " + totalQuantity);
                    // if (!backroomPickedQuantity.trim().equals("")) {
                    //Divya - Changed this to calculate quantity to be picked
                    qtyToPick = ((double) (Double.parseDouble(totalQuantity))) -((double) (Double.parseDouble(backroomPickedQuantity)));
                    //}
                    log.info("To be picked quantity is " + qtyToPick);

                    itemInfoMap.put("qtyToPick", "" + qtyToPick);
                    pickList.add(itemInfoMap);

                    pickCount++;
                }
                vars.putObject("batchPickMap", batchPickMap);
                vars.putObject("pickList", pickList);
/*                if(totalNumberOfRecords != null) {
                    vars.put("numberOfItemsToPick", totalNumberOfRecords);
                }else{
                    // TODO finish this
                    totalNumberOfRecords = totalNumberOfRecords +
                    vars.put("numberOfItemsToPick", totalNumberOfRecords);

                }*/
                vars.put("isLastPage", isLastPage);
                //store last record in case we need to navigate to next page
                log.info("mapis " + batchPickMap+" and list is " + pickList);
                if (pickList.size() == 0) {
                    //Set all variables for next request can be next page or a filter
                    controllerName = "next_page_controller";
                    //controllerName = "filter_page_controller" ;
                } else if (isLastPage.equals("N")) {
                    //Divya - Added this as the batchNo wasnt being set to the new one
                    log.info("batch number is " + batchNo);
                    vars.put("batchNo", batchNo);
                    log.info("store batch key is " + storeBatchKey);

                    vars.put("storeBatchKey", storeBatchKey);
                    controllerName = "next_prod_page_controller";
                } else if (isLastPage.equals("Y")) {
                    vars.put("numberOfItemsToPick", ""+pickList.size());
                    controllerName = "perform_pick_controller";
                    vars.put("itemPickCount", "" + 0);
                    vars.put("incrementCounter", "" + 1);
                }
                log.info("controller name is " + controllerName);

            }
            vars.put("lastRecordOnPage", "" + lastRecordOnPage);
            vars.put("pickOneRequest", "" + pickOneRequest);
            vars.put("controllerName", controllerName);
            return "OK";
        }

        return isJunkOrder;
    }
    public String isValidJsonResponse(JMeterVariables vars, Log4jLogger log, JsonObject jsonObject) {
        String isValid = "false";
        if (jsonObject != null) {

           if (jsonObject.get("controllerData") == null) {
                vars.put("requestName", "ERROR");
                //to exit out in case of error
                vars.put("incrementCounter", "" + 10000);
                String currentOrderNo = vars.get("currentOrderNumber");

               Map lastOrderNumberMap = (Map) (vars.getObject("failedOrderNumbers"));

               if (lastOrderNumberMap == null) {
                    lastOrderNumberMap = new HashMap();
                } else {
                    lastOrderNumberMap.put(currentOrderNo, currentOrderNo);
                }
                vars.putObject("failedOrderNumbers", lastOrderNumberMap);


           } else {
                isValid = "true";
            }
        }
        return isValid;
    }
    public String processLogin(JMeterVariables vars, Log4jLogger log, JsonObject jsonObject) {
    	log.info("in process login");
        int pickCount = 0;
        JsonArray pickShipmentArray = null;
        String isLastPage = null;
        JsonObject lastRecordOnPage = null;
        if(vars.get("requestName").equals("LOGIN")){
        	 pickShipmentArray = (jsonObject.get("controllerData").asObject().get("wsc_components_batch_batchlist_BatchPickListInitController").asObject().get("batchPick_getBatchListForStore").asObject().get("Page").asObject().get("Output").asObject().get("StoreBatchList").asObject().get("StoreBatch")).asArray();
        

         lastRecordOnPage = jsonObject.get("controllerData").asObject().get("wsc_components_batch_batchlist_BatchPickListInitController").asObject().get("batchPick_getBatchListForStore").asObject().get("Page").asObject().get("LastRecord").asObject();
         isLastPage = jsonObject.get("controllerData").asObject().get("wsc_components_batch_batchlist_BatchPickListInitController").asObject().get("batchPick_getBatchListForStore").asObject().get("Page").asObject().get("IsLastPage").asString();
        }else if(vars.get("requestName").equals("NEXTPAGE")){
          	 pickShipmentArray = (jsonObject.get("controllerData").asObject().get("wsc_components_batch_batchlist_BatchPickListBehaviorController").asObject().get("batchPick_getBatchListForStore_behavior").asObject().get("Page").asObject().get("Output").asObject().get("StoreBatchList").asObject().get("StoreBatch")).asArray();
            

             lastRecordOnPage = jsonObject.get("controllerData").asObject().get("wsc_components_batch_batchlist_BatchPickListBehaviorController").asObject().get("batchPick_getBatchListForStore_behavior").asObject().get("Page").asObject().get("LastRecord").asObject();
             isLastPage = jsonObject.get("controllerData").asObject().get("wsc_components_batch_batchlist_BatchPickListBehaviorController").asObject().get("batchPick_getBatchListForStore_behavior").asObject().get("Page").asObject().get("IsLastPage").asString();
               	
        }
        //last record on page details - woudl be requied if next page is called


        //Iterate through array to get a shipment not assigned to another user

        String assignedToUserID = null;
        String storeBatchKey = null;
        String newRequest = null;
        String currentOrderNo = null;
        String pickStatus = null;
        String username = vars.get("username");

        // TODO - review
        Map mapFailedOrderNumbers = (Map) (vars.getObject("failedOrderNumbers"));
        if (mapFailedOrderNumbers == null) {
            log.info(String.format("creating failedOrderNumbers map for %s -- which should only happen once per user", username));
            mapFailedOrderNumbers = new HashMap();
            vars.putObject("failedOrderNumbers", mapFailedOrderNumbers);
        }

        String batchNo = null;
        String storeBatchConfigList = null;
        String imageAltText = null;
        String imageUrl = null;
        String oldestExpShpDate = null;
        JsonObject pickRequest = null;
        String pickBatchDetails = null;
        String pickOneRequest = null;
        String controllerName = null;
        log.info("Pick shipmenta array " + pickShipmentArray.get(pickCount) + " and pick count is " + pickCount);
        while (pickShipmentArray!=null && pickCount < pickShipmentArray.size()) {
            log.info("in while");
            if (pickShipmentArray.get(pickCount).asObject().get("AssignedToUsername") != null) {
                assignedToUserID = pickShipmentArray.get(pickCount).asObject().get("AssignedToUsername").asString();
                storeBatchKey = (pickShipmentArray.get(pickCount).asObject().get("StoreBatchKey")).asString();
                newRequest = "N";
                log.info("in if");
                currentOrderNo = (pickShipmentArray.get(pickCount).asObject().get("BatchNo")).asString();

                //currentOrderNo = "";
            } else {
                assignedToUserID = vars.get("username");
                if (pickShipmentArray.get(pickCount).asObject().get("StoreBatchKey") != null) {
                    log.info("its not a new request");
                    storeBatchKey = (pickShipmentArray.get(pickCount).asObject().get("StoreBatchKey")).asString();
                    newRequest = "N";
                    //commented on 06/06
                    currentOrderNo = "";//(pickShipmentArray.get(pickCount).asObject().get("BatchNo")).asString();
                } else {
                    storeBatchKey = null;
                    newRequest = "Y";
                    log.info("its a new request");

                    currentOrderNo = "";
                }
            }

            pickStatus = pickShipmentArray.get(pickCount).asObject().get("Status").asString();
            log.info("after pick status");
            log.info("after last order map" + currentOrderNo + "last order map" + mapFailedOrderNumbers);
            if ((currentOrderNo != null && mapFailedOrderNumbers.get(currentOrderNo) == null) && (pickStatus.equals("1000") || pickStatus.equals("1100")) && (assignedToUserID.equals(vars.get("username")))) {
                log.info("getting the order");
            	batchNo = (pickShipmentArray.get(pickCount).asObject().get("BatchNo")).asString();
                storeBatchConfigList = pickShipmentArray.get(pickCount).asObject().get("StoreBatchConfigList").toString();
                imageAltText = pickShipmentArray.get(pickCount).asObject().get("ImageAltText").asString();
                imageUrl = pickShipmentArray.get(pickCount).asObject().get("ImageUrl").asString();
                oldestExpShpDate = pickShipmentArray.get(pickCount).asObject().get("OldestExpShpDate").asString();
                pickRequest = pickShipmentArray.get(pickCount).asObject();
                pickBatchDetails = pickRequest.get("ShipmentLines").toString();

                //this forces the loop to exit
                pickCount = pickShipmentArray.size();
            }

            log.info("after last");
            //added on 06/06
            if(!currentOrderNo.equals("")){
            	mapFailedOrderNumbers.put(currentOrderNo, currentOrderNo);
            	vars.putObject("failedOrderNumbers", mapFailedOrderNumbers);
            }
            pickCount++;
            //store last record in case we need to navigate to next page
        }

        if (pickRequest != null) {
	            /*	pickOneRequest = "{\"scWizardDefId\":\"extn.components.batch.batchpick.sortwhilepick.SortWhilePickupWizardExtn\",\"isWizard\":true,\"scControllerInput\":{\"StoreBatch\":{\"Status\":\""+pickStatus+"\",\"StoreBatchKey\":\""+storeBatchKey.trim()+"\",\"BatchNo\":\""+batchNo+"\",\"ShipmentLines\":"+pickBatchDetails +"},"+"\"StoreBatchConfigList\":"+ storeBatchConfigList.toString() +",\"BatchType\":\""+batchType+"\",\"ImageAltText\":\""+imageAltText+"\",\"ImageUrl\":\""+imageUrl+"\",\"OldestExpShpDate\":\""+oldestExpShpDate+"\"}}}";*/
            if (newRequest.equals("N")) {
                pickOneRequest = "{\"scWizardDefId\":\"extn.components.batch.batchpick.sortwhilepick.SortWhilePickupWizardExtn\",\"isWizard\":true,\"scControllerInput\":{\"StoreBatch\":{\"Status\":\"" + pickStatus + "\",\"BatchNo\":\"" + batchNo + "\", \"BatchType\": \"SORT_AFTER_PICK\",\"ShipmentLines\":" + pickBatchDetails + ",\"StoreBatchConfigList\":" + storeBatchConfigList + ",\"StoreBatchKey\": \"" + storeBatchKey + "\", \"ImageAltText\":\"" + imageAltText + "\",\"OldestExpShpDate\":\"" + oldestExpShpDate + "\"}}}}";
            } else {
                pickOneRequest = "{\"scWizardDefId\":\"extn.components.batch.batchpick.sortwhilepick.SortWhilePickupWizardExtn\",\"isWizard\":true,\"scControllerInput\":{\"StoreBatch\":{\"Status\":\"" + pickStatus + "\",\"BatchNo\":" + batchNo + ",\"ShipmentLines\":" + pickBatchDetails + ",\"StoreBatchConfigList\":" + storeBatchConfigList + ",\"ImageAltText\":\"" + imageAltText + "\",\"OldestExpShpDate\":\"" + oldestExpShpDate + "\"}}}";
            }
            controllerName = "pick_controller";

        } else {
            if (isLastPage.equals("Y")) {
                //int exitCounter = 9999;
               // vars.put("exitCounter", "" + exitCounter);
                controllerName = "home_controller";
                vars.put("workavailable","no");
            } else {
//                batchNo = lastRecordOnPage.get("StoreBatch").asObject().get("BatchNo").asString();
//                pickStatus = lastRecordOnPage.get("StoreBatch").asObject().get("Status").asString();
                controllerName = "next_page_controller";
            }
        }

        vars.put("lastRecordOnPage", "" + lastRecordOnPage);
        vars.put("pickOneRequest", "" + pickOneRequest);
        vars.put("controllerName", controllerName);
        return "OK";
    }
}