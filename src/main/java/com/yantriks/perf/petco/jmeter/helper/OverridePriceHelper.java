package com.yantriks.perf.petco.jmeter.helper;

import org.apache.jmeter.samplers.SampleResult;
import org.apache.jmeter.threads.JMeterVariables;
//import org.apache.logging.slf4j.Log4jLogger;
import org.apache.logging.slf4j.Log4jLogger;

import com.eclipsesource.json.JsonObject;
import com.eclipsesource.json.JsonArray;
import com.jayway.jsonpath.*;
import com.jayway.jsonpath.spi.mapper.*;

import net.minidev.json.JSONObject;

import com.jayway.jsonpath.spi.json.*;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.LinkedHashMap;
import java.util.List;

public class OverridePriceHelper {
	
	public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException {
	}

	private static final Configuration configuration = Configuration.builder()
			.jsonProvider(new JacksonJsonNodeJsonProvider()).mappingProvider(new JacksonMappingProvider()).build();

//	public void sayHello(Log4jLogger log) {
	public void sayHello(Log4jLogger log) {
		log.info("OverridePriceHelper v1.19");
	}

//	public String processWCCLogin(JMeterVariables vars, Log4jLogger logger) {
		public String processWCCLogin(JMeterVariables vars, Log4jLogger logger) {

		String requestName = vars.get(WCCConst.VK_REQUEST_NAME);

		if (requestName.equals("ERROR")) {
			vars.put(WCCConst.VK_CONTROLLER_NAME, WCCConst.VV_ORDER_ERROR_CONTROLLER);
		} else if (requestName.equals(WCCConst.VV_LOGIN_REQUEST)) {
			vars.put(WCCConst.VK_CONTROLLER_NAME, WCCConst.VV_SEARCH_ORDER_CONTROLLER);
		}

		return "OK";
	}
	
	

//	public void processSearchOrder(JMeterVariables vars, Log4jLogger logger) {
		public void processSearchOrder(JMeterVariables vars, Log4jLogger logger) {
		
		String responseJson = vars.get("responseJson");
		String requestName = vars.get("requestName");
		String orderNo = vars.get("orderNo");

		if (requestName.equals("ERROR")) {
			vars.put(WCCConst.VK_CONTROLLER_NAME, WCCConst.VV_ORDER_ERROR_CONTROLLER);

		} else if (requestName.equals("ORDER_PORTLET")) {

			//vars.put(WCCConst.VK_CONTROLLER_NAME, WCCConst.VV_SEARCH_ORDER_CONTROLLER);
			String updatedJson = JsonPath.using(configuration)
					.parse(sampOrderSearchJson.jsonReqOrderPortletBehaviourController)
					.set("$.isccs_home_portlets_OrderPortletBehaviorController.MashupRefs.MashupRef[0].Input.Order.OrderNo",
							orderNo)
					.jsonString().toString();
			vars.put("requestJson", updatedJson.toString());
			// logger.info(updatedJson.toString());

		} else if (requestName.equals("WIZARD")) {

			//vars.put(WCCConst.VK_CONTROLLER_NAME, WCCConst.VV_SEARCH_ORDER_CONTROLLER);
			//Store the order level attributes
			vars.put("orderAttr", JsonPath.read(responseJson,"$..Order.[0]").toString());

			List<String> orders = JsonPath.read(responseJson,
					"$.controllerData.isccs_home_portlets_OrderPortletBehaviorController.getOrderList.OrderList.Order");
			vars.put("requestJson", orders.toString().replace("[", "").replace("]", ""));
			//logger.info("request  for wizard" + orders);

		} else if (requestName.equals("ORDER_EDIT_BEHAVIOUR")) {

			String orders = JsonPath.parse(responseJson).read("$.controllerData.isccs_order_details_OrderSummaryInitController.getCompleteOrderDetails.Order.OrderHeaderKey");
			//JsonPath.read(responseJson,
			//"$.controllerData.isccs_order_details_OrderSummaryInitController.getCompleteOrderDetails.Order.OrderHeaderKey");
			logger.info("request  for edit before replacing" + orders);

			vars.put("requestJson", orders.toString().replace("[", "").replace("]", ""));
			vars.put("orderHeaderKey", orders.toString().replace("[", "").replace("]", ""));
			//logger.info("request  for edit" + orders);

		}
	}

//	public void processLineSummary(JMeterVariables vars, Log4jLogger logger) {
		public void processLineSummary(JMeterVariables vars, Log4jLogger logger) {

		String responseJson = vars.get("responseJson");
		String requestName = vars.get("requestName");
		String orderNo = vars.get("orderNo");


		if (requestName.equals("ERROR")) {
			//vars.put(WCCConst.VK_CONTROLLER_NAME, WCCConst.VV_ORDER_ERROR_CONTROLLER);

		} else if (requestName.equals("ORDER_LINE_SUMMARY_WIZARD")) {
			String orderAttributes = vars.get("responseJson");

			//vars.put(WCCConst.VK_CONTROLLER_NAME, WCCConst.VV_SEARCH_ORDER_CONTROLLER);
			String currency = JsonPath.read(orderAttributes,
					"$..Output.OrderLineList.Order.PriceInfo.Currency").toString().replace("[", "").replace("]", "").replace("\"", "");
			String  ordHdrKey = JsonPath.read(orderAttributes, 
					"$..Output.OrderLineList.Order.OrderHeaderKey").toString().replace("[", "").replace("]", "").replace("\"", "");
			String  docType = JsonPath.read(orderAttributes, 
					"$.controllerData..Order.DocumentType").toString().replace("[", "").replace("]", "").replace("\"", "");
			String  ordNo = JsonPath.read(orderAttributes, 
					"$.controllerData..Order.OrderNo").toString().replace("[", "").replace("]", "").replace("\"", "");			

			String entryType = JsonPath.read(orderAttributes, 
					"$.controllerData..Order.EntryType").toString().replace("[", "").replace("]", "").replace("\"", "");			

			String sellOrgCode = JsonPath.read(orderAttributes, 
					"$.controllerData..Order.SellerOrganizationCode").toString().replace("[", "").replace("]", "").replace("\"", "");	
			String custFirstName =JsonPath.read(orderAttributes, 
					"$.controllerData..Order.CustomerFirstName").toString().replace("[", "").replace("]", "").replace("\"", "");	
			String custLastName = JsonPath.read(orderAttributes, 
					"$.controllerData..Order.CustomerLastName").toString().replace("[", "").replace("]", "").replace("\"", "");	

			String orderLnKey =  JsonPath.read(orderAttributes, 
					"$..Output.OrderLineList.OrderLine.[0].OrderLineKey").toString().replace("[", "").replace("]", "").replace("\"", "");
			String updatedJson = JsonPath.using(configuration)
					.parse(sampOrderSearchJson.jsonReqViewLineWizardController)
					.set("$..PriceInfo.Currency",
							currency)
					.jsonString().toString();
			updatedJson = JsonPath.using(configuration)
					.parse(updatedJson)
					.set("$..OrderHeaderKey",
							ordHdrKey)
					.jsonString().toString();
			updatedJson = JsonPath.using(configuration)
					.parse(updatedJson)
					.set("$..OrderNo",
							orderNo)
					.jsonString().toString();	
			updatedJson = JsonPath.using(configuration)
					.parse(updatedJson)
					.set("$..OrderLineKey",orderLnKey)
					.jsonString().toString();
			updatedJson = JsonPath.using(configuration)
					.parse(updatedJson)
					.set("$..DocType",docType)
					.jsonString().toString();	
			updatedJson = JsonPath.using(configuration)
					.parse(updatedJson)
					.set("$..EntryType",entryType)
					.jsonString().toString();
			updatedJson = JsonPath.using(configuration)
					.parse(updatedJson)
					.set("$..SellerOrganizationCode",sellOrgCode)
					.jsonString().toString();			
			updatedJson = JsonPath.using(configuration)
					.parse(updatedJson)
					.set("$..CustomerFirstName",custFirstName)
					.jsonString().toString();
			updatedJson = JsonPath.using(configuration)
					.parse(updatedJson)
					.set("$..CustomerFirstName",custLastName)
					.jsonString().toString();			
			vars.put("orderLineKey", orderLnKey);
			
			//List<String> orders = JsonPath.read(responseJson,
			//		"$.controllerData.isccs_home_portlets_OrderPortletBehaviorController.getOrderList.OrderList.Order");
			vars.put("requestJson", updatedJson);

		}  else if (requestName.equals("ORDER_LINE_EDIT_BEHAVIOUR")) {
			//vars.put(WCCConst.VK_CONTROLLER_NAME, WCCConst.VV_SEARCH_ORDER_CONTROLLER);
			vars.put("orderLineKey", JsonPath.read(responseJson,"$.controllerData..getCompleteOrderLineDetails.OrderLine.OrderLineKey").toString().replace("[", "").replace("]", "").replace("\"", ""));

			String orders = JsonPath.read(responseJson,
					"$.controllerData.isccs_order_details_OrderLineSummaryInitController.getCompleteOrderLineDetails.OrderLine.Order.OrderHeaderKey");
			vars.put("requestJson", orders.toString().replace("[", "").replace("]", ""));
			System.out.println("order is" + orders.toString());

		} else if (requestName.equals("ORDER_SUMMARY_WIZARD")) {

			//vars.put(WCCConst.VK_CONTROLLER_NAME, WCCConst.VV_SEARCH_ORDER_CONTROLLER);
			//Store the order level attributes
			 String orders = vars.get("orderAttr");

			//List<String> orders = JsonPath.read(responseJson,
			//		"$.controllerData.isccs_home_portlets_OrderPortletBehaviorController.getOrderList.OrderList.Order");
			//String orders = JsonPath.read(responseJson,
					//"$.controllerData.isccs_editors_OrderEditorBehaviorController.getRequiredEditorAttributes");
			vars.put("requestJson", orders.toString().replace("[", "").replace("]", ""));
			//logger.info("request  for wizard" + orders);

		}

	}


//	public void processOrderSummary(JMeterVariables vars, Log4jLogger logger) {
		public void processOrderSummary(JMeterVariables vars,Log4jLogger logger) {

		String responseJson = vars.get("responseJson");
		String requestName = vars.get("requestName");

		if (requestName.equals("ERROR")) {
			vars.put(WCCConst.VK_CONTROLLER_NAME, WCCConst.VV_ORDER_ERROR_CONTROLLER);

		} else if (requestName.equals("ORDER_SUMMARY_WIZARD") ){
			//vars.put(WCCConst.VK_CONTROLLER_NAME, WCCConst.VV_SEARCH_ORDER_CONTROLLER);
			//Store the order level attributes
			 String orders = vars.get("orderAttr");

			//List<String> orders = JsonPath.read(responseJson,
			//		"$.controllerData.isccs_home_portlets_OrderPortletBehaviorController.getOrderList.OrderList.Order");
			//String orders = JsonPath.read(responseJson,
					//"$.controllerData.isccs_editors_OrderEditorBehaviorController.getRequiredEditorAttributes");
			vars.put("requestJson", orders.toString().replace("[", "").replace("]", ""));
			//logger.info("request  for wizard" + orders);
			


		}else if (requestName.equals("ORDER_EDITOR_BEHAVIOUR")) {
			String orders = JsonPath.parse(responseJson).read("$.controllerData.isccs_order_details_OrderSummaryInitController.getCompleteOrderDetails.Order.OrderHeaderKey");
			//JsonPath.read(responseJson,
			//"$.controllerData.isccs_order_details_OrderSummaryInitController.getCompleteOrderDetails.Order.OrderHeaderKey");
			vars.put("requestJson", orders.toString().replace("[", "").replace("]", ""));
			vars.put("orderHeaderKey", orders.toString().replace("[", "").replace("]", ""));
			//logger.info("request  for edit" + orders);;	
			Object orderLineSelected = JsonPath.read(responseJson,
					"$..OrderLine.[0]");
			vars.put("orderLineSelected", orderLineSelected.toString().replace("[", "").replace("]", ""));
			System.out.println(orderLineSelected);
			
		}
	}


//	public void processAddLine(JMeterVariables vars, Log4jLogger logger) {
		public void processAddLine(JMeterVariables vars, Log4jLogger logger) {

		String responseJson = vars.get("responseJson");
		String requestName = vars.get("requestName");

		if (requestName.equals("ERROR")) {
			vars.put(WCCConst.VK_CONTROLLER_NAME, WCCConst.VV_ORDER_ERROR_CONTROLLER);

		} else if (requestName.equals("ORDER_SUMMARY_WIZARD") ){
			//Object orders = JsonPath.parse(responseJson).read("$.controllerData.isccs_editors_OrderEditorBehaviorController.getRequiredEditorAttributes.Order");
			//vars.put("requestJson", orders.toString().replace("[", "").replace("]", "").replace("=", ":"));
			
			//vars.put(WCCConst.VK_CONTROLLER_NAME, WCCConst.VV_SEARCH_ORDER_CONTROLLER);
			//Store the order level attributes
			 String orders = vars.get("orderAttr");

			//List<String> orders = JsonPath.read(responseJson,
			//		"$.controllerData.isccs_home_portlets_OrderPortletBehaviorController.getOrderList.OrderList.Order");
			//String orders = JsonPath.read(responseJson,
					//"$.controllerData.isccs_editors_OrderEditorBehaviorController.getRequiredEditorAttributes");
			vars.put("requestJson", orders.toString().replace("[", "").replace("]", ""));
			//logger.info("request  for wizard" + orders);
			


		}else if (requestName.equals("ADD_LINES_TO_ORDER")) {
			//Object orders = JsonPath.parse(responseJson).read("$.controllerData.isccs_order_details_OrderSummaryInitController.getCompleteOrderDetails");
			//JsonPath.read(responseJson,
			LinkedHashMap ordersMap = JsonPath.parse(responseJson).read("$.controllerData.isccs_order_details_OrderSummaryInitController.getCompleteOrderDetails");
			String orders = new JSONObject(ordersMap).toString();
			LinkedHashMap personInfoMap = JsonPath.parse(responseJson).read("$.controllerData.isccs_order_details_OrderSummaryInitController.getCompleteOrderDetails.Order.PersonInfoShipTo");
			String personInfo = new JSONObject(personInfoMap).toString();
			String addLinesJson = orders.substring(1, orders.length()-2).concat(",\"RelatedTaskClicked\":\"AddLinesToOrderWizard\"");
			//orders = orders.substring(1, orders.length()-1);
			//"$.controllerData.isccs_order_details_OrderSummaryInitController.getCompleteOrderDetails.Order.OrderHeaderKey");
			vars.put("requestJson", orders.substring(1, orders.length()-1));
			vars.put("orderDetails", orders.substring(1, orders.length()-1));
			vars.put("personInfoShipTo",personInfo);
			vars.put("addLinesJson",addLinesJson);
		
			
		}
	}

	
//	public void verifyResponse(JMeterVariables vars, Log4jLogger logger,SampleResult sampRes) {
		public void verifyResponse(JMeterVariables vars, Log4jLogger logger,SampleResult sampRes) {
		
		String responseJson = vars.get("responseJson");	
		String requestName = vars.get("requestName");
		String controllerName = vars.get("controllerName");		
		if ( (!responseJson.contains("screenId"))) {
			if (responseJson.contains("Error") || (responseJson.contains("error"))){
				vars.put("failedControllerName", controllerName);
				vars.put("failedRequestName", requestName);
				vars.put("failedRequest", sampRes.getResponseDataAsString());
				vars.put("failedResponse", sampRes.getRequestHeaders());				
				vars.put("controllerName", "DUMP_THE_WORLD");
				sampRes.setSuccessful(false);
				sampRes.setResponseCode("0007");
			}
		}
		logger.info("Here in verify");
		
	}

}
