package com.yantriks.perf.petco.jmeter.helper;

import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import org.apache.jmeter.threads.JMeterVariables;
import org.apache.logging.slf4j.Log4jLogger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by berniewong on 5/10/17.
 */
public class PackHelper {

	public void sayHello(Log4jLogger log) {
		log.info("PackHelper v0.15");
	}

	public String processPackResponse(JMeterVariables vars, Log4jLogger log) {
		//Log log = new Log(logger);

		String responseJson = vars.get(SFSConst.VK_RESPONSE_JSON);
		String requestName = vars.get(SFSConst.VK_REQUEST_NAME);
		JsonObject jsonObject = new JsonObject() ;
		log.info("request name is " + requestName);
		/*if(JsonObject.readFrom(responseJson)==null){
			vars.put("requestName","ERROR");
			String controllerName = "dump_the_world";	
			//to exit out in case of error
		}*/
		if (requestName.equals("ERROR")) {
			vars.put("controllerName", "dump_the_world");
			return "OK";
		}else if (requestName.equals("LOGIN_TOKEN")) {
			vars.put("controllerName", "home_controller");

		} else if (requestName.equals("DUMP")) {
			vars.put("controllerName", "home_controller");
			return "OK";

		} else if (requestName.equals("MOVE")) {
			vars.put("controllerName", "next_page_controller");

		} else if (requestName.equals("FINISHEDPACK")) {
			int totalNumberOfProductPages = 1;
			//String prodPageNumber = vars.get("prodPageNumber");
			///String isValidProdPage = vars.get("isValidProdPage");
			/*   if(vars.get("totalNumberOfProdPages")!=null) {
                totalNumberOfProductPages = Integer.parseInt(vars.get("totalNumberOfProdPages"));
                //TODO review if this is used
                prodPageNumber = vars.get("prodPageNumber");
                isValidProdPage = vars.get("isValidProdPage");
            }*/

			// TODO what is total > 1 or < 0
			// TODO handle null and other conditions
			/*if ( (totalNumberOfProductPages > 1 && isValidProdPage.equals("Y") && !prodPageNumber.equals(totalNumberOfProductPages)) ){
                vars.put("controllerName", "next_product_page_controller");
                return "OK";

            } else{*/
			//  vars.put("prodPageNumber", "");
			// TODO why is this commented
			vars.put("controllerName", "finish_pack");
			return "OK";
			// }
			// TODO please  check
		} else if (requestName.equals("PACK")){
				jsonObject=JsonObject.readFrom(responseJson);

			performPackInPack(vars, log, jsonObject);
		} else if(requestName.equals("NEXTPAGE")||(requestName.equals("FILTERPAGE"))||(requestName.equals("LOGIN"))){
			log.info("in login condition");
			jsonObject=JsonObject.readFrom(vars.get("responseJson"));
			log.info("After in login condition");

			performSelectFromPackList(vars, log, jsonObject);
		}else {
			// TODO handle the case where none of the request names matched
			vars.put("controllerName", "dump_the_world");
			return "OK";


		}

		return "OK";
	}

	private void performSelectFromPackList(JMeterVariables vars, Log4jLogger log, JsonObject jsonObject){
		String isLastEverPage = null;
		String lastOrderNummber;
		String homePageNumber = "";

		String requestName = vars.get(SFSConst.VK_REQUEST_NAME);
		JsonArray pickShipmentArray = null;
		JsonObject lastRecordOnPage = null;

		if (requestName.equals("NEXTPAGE") || (requestName.equals("FILTERPAGE"))) {
			pickShipmentArray = (jsonObject.get("controllerData").asObject().get("wsc_mobile_home_subscreens_OrdersReadyForPackingBehaviorController").asObject().get("getShipmentListInitBehavior").asObject().get("Page").asObject().get("Output").asObject().get("Shipments").asObject().get("Shipment")).asArray();

			lastRecordOnPage = jsonObject.get("controllerData").asObject().get("wsc_mobile_home_subscreens_OrdersReadyForPackingBehaviorController").asObject().get("getShipmentListInitBehavior").asObject().get("Page").asObject().get("LastRecord").asObject();

			// TODO is homePageNumber used???
			homePageNumber = jsonObject.get("controllerData").asObject().get("wsc_mobile_home_subscreens_OrdersReadyForPackingBehaviorController").asObject().get("getShipmentListInitBehavior").asObject().get("Page").asObject().get("PageNumber").asString();

			isLastEverPage=jsonObject.get("controllerData").asObject().get("wsc_mobile_home_subscreens_OrdersReadyForPackingBehaviorController").asObject().get("getShipmentListInitBehavior").asObject().get("Page").asObject().get("IsLastPage").asString();
		}else if(requestName.equals("LOGIN")){

			isLastEverPage = jsonObject.get("controllerData").asObject().get("wsc_mobile_home_subscreens_OrdersReadyForPackingInitController").asObject().get("getShipmentList").asObject().get("Page").asObject().get("IsLastPage").asString();
			log.info("******before the last page check and teh value is " + isLastEverPage);
			//if (isLastEverPage.equals("N")) {//last page logic commented
			//code for delaying login
			if(jsonObject.get("controllerData").asObject().get("wsc_mobile_home_subscreens_OrdersReadyForPackingInitController").asObject().get("getShipmentList").asObject().get("Page").asObject().get("Output").asObject().get("Shipments").asObject().get("Shipment") != null){
				pickShipmentArray=(jsonObject.get("controllerData").asObject().get("wsc_mobile_home_subscreens_OrdersReadyForPackingInitController").asObject().get("getShipmentList").asObject().get("Page").asObject().get("Output").asObject().get("Shipments").asObject().get("Shipment")).asArray();
			}else{
				vars.put("controllerName","sleep");
				
			}
				lastRecordOnPage = jsonObject.get("controllerData").asObject().get("wsc_mobile_home_subscreens_OrdersReadyForPackingInitController").asObject().get("getShipmentList").asObject().get("Page").asObject().get("LastRecord").asObject();

				homePageNumber = jsonObject.get("controllerData").asObject().get("wsc_mobile_home_subscreens_OrdersReadyForPackingInitController").asObject().get("getShipmentList").asObject().get("Page").asObject().get("PageNumber").asString();
			//}
			log.info("******after the last page check and the value of pickshiparray is " + pickShipmentArray);

		}

		int pickCount = 0;
		String assignedToUserID = null;
		String controllerName = null;
		Map lastOrderNumberMap = new HashMap();
		JsonObject pickRequest=null;
		String pickStatusDesc = null;
		String pickOneRequest = null;

		// Iterate through array to get a shipment not assigned to another user
		//while(isLastEverPage.equals("N") && pickCount<pickShipmentArray.size()){
		while(pickShipmentArray != null && pickCount<pickShipmentArray.size()){//removed the last page check
			// Read status of each shipment and pick the one that has status "Ready To
			// Pick";
			log.info("***** inside while");
			String currentOrderNo = pickShipmentArray.get(pickCount).asObject().get("DisplayOrderNo").asString();

			String pickStatus = pickShipmentArray.get(pickCount).asObject().get("Status").asObject().get("Status").asString();

			pickStatusDesc = pickShipmentArray.get(pickCount).asObject().get("Status").asObject().get("Description").asString();
			// the item could be in progress or a new batch and based on that there would or
			// would not be a value for assignedtouserid
			log.info("***** inside ");
			if (pickShipmentArray.get(pickCount).asObject().get("AssignedToUserId")!=null) {
				assignedToUserID = pickShipmentArray.get(pickCount).asObject().get("AssignedToUserId").asString();
				controllerName = "pack_controller_continue";
			} else if (pickShipmentArray.get(pickCount).asObject().get("AssignedToUserId")==null){
				assignedToUserID="";
				//controllerName="pack_controller_continue";
			}


			String lastOrderOnPage = lastRecordOnPage.get("Shipment").asObject().get("DisplayOrderNo").asString();
			vars.put("lastOrderOnPage",lastOrderOnPage);
			//TODO remove unused variables
			String lastOrderNumber = vars.get("lastOrderNumber");
			
			lastOrderNumberMap = (Map) (vars.getObject("failedOrderNumbers"));
			log.info("The MAP is" + vars.getObject("failedOrderNumbers") + "current is" + lastOrderNumberMap.get(currentOrderNo));
			if(((lastOrderNumberMap == null || lastOrderNumberMap.get(currentOrderNo)== null))&&(pickStatus.equals("1100.70.06.50")||(pickStatus.equals("1100.70.06.70.5")))&&((assignedToUserID.equals(vars.get("username"))||(assignedToUserID.equals(""))))){

				pickRequest = pickShipmentArray.get(pickCount).asObject();
				// this forces the loop to exit
				if(pickShipmentArray.get(pickCount).asObject().get("ShipmentKey") != null){
					String shipmentKey = pickShipmentArray.get(pickCount).asObject().get("ShipmentKey").asString();
					vars.put("shipKey", ""+shipmentKey);
				}
				pickCount=pickShipmentArray.size();


				vars.put("currentOrderNumber",currentOrderNo);
			}
			if(lastOrderNumberMap == null){
				lastOrderNumberMap = new HashMap();
			}else {
				lastOrderNumberMap.put(currentOrderNo, currentOrderNo);
			}
			vars.putObject("failedOrderNumbers",lastOrderNumberMap);
			pickCount++;
		}

		if((pickRequest==null)&&(isLastEverPage.equals("N"))){
			// Set all variables for next request can be next page or a filter
			controllerName = "next_page_controller" ;

		}if((pickRequest==null)&&(isLastEverPage.equals("Y"))){
			// Set all variables for next request can be next page or a filter
			//code for delaying login
			if(!controllerName.equals("sleep"))
			controllerName = "home_controller" ;

		}/*else if((pickRequest==null)&&(isLastEverPage.equals("Y")))//removed this condition
			log.info("here in exit counter");
			vars.put("exitCounter","-1");

		}*/else if(pickRequest!=null){//the shipment can be picked. so create request for
			//TODO check why there were conditions earlier
			// if(assignedToUserID.equals("")||assignedToUserID.equals(vars.get("username"))){
			//controllerName = "pack_controller_new";
			controllerName = "pack_controller_continue";
			////    }else if (pickStatusDesc.equals("Ready for packing")&& (!assignedToUserID.equals(""))){
			//        controllerName = "items_packed";
			// }else if(pickStatusDesc.equals("Pack in progress")){
			//       controllerName = "pack_controller_continue";
			//     }

			pickOneRequest = "" + pickRequest;
		}


	/*	if((pickRequest==null)&&(isLastEverPage.equals("Y"))){//Is this used?
			log.info("here in exit counter");
			vars.put("exitCounter","-1");

		}*/
		log.info("here outside else if");
		vars.put("lastRecordOnPage",""+lastRecordOnPage);
		vars.put("pickRequest",""+pickRequest);
		vars.put("pickOneRequest",""+pickOneRequest);
		vars.put("controllerName",""+controllerName);

	}

	private String performPackInPack(JMeterVariables vars, Log4jLogger log, JsonObject jsonObject) {
		String controllerName = "invalid";

		boolean allPacked = false;
		JsonArray pickShipmentArray = null;
		JsonObject pickShipmentNode = null;
		String isValidPage = "";

		

		if (jsonObject.get("controllerData").asObject().get("wsc_components_shipment_container_pack_ContainerPackInitController") != null) {

			pickShipmentArray = jsonObject.get("controllerData").asObject().get("wsc_components_shipment_container_pack_ContainerPackInitController").asObject().get("getShipmentDetails").asObject().get("Shipment").asObject().get("ShipmentLines").asArray();
			pickShipmentNode = jsonObject.get("controllerData").asObject().get("wsc_components_shipment_container_pack_ContainerPackInitController").asObject().get("getShipmentDetails").asObject().get("Shipment").asObject().get("ShipmentLines").asObject().get("TotalNumberOfRecords").asObject();


			String shipContainerScm = jsonObject.get("controllerData").asObject().get("wsc_components_shipment_container_pack_ContainerPackInitController").asObject().get("getShipmentDetails").asObject().get("Shipment").asObject().get("Container").asObject().get("ContainerScm").asString();
			String shipmentkey =jsonObject.get("controllerData").asObject().get("wsc_components_shipment_container_pack_ContainerPackInitController").asObject().get("getShipmentDetails").asObject().get("Shipment").asObject().get("ShipmentKey").asString();


			vars.put("containerScm",shipContainerScm);
			vars.put("shipKey",shipmentkey);
			//controllerName = SFSConst.VV_PERFORM_PACK_CONTROLLER;
			//                return "OK";
		} else if (jsonObject.get("controllerData").asObject().get("wsc_components_shipment_container_pack_ContainerPackProductListBehaviorController").asObject().get("containerPack_getShipmentLineList_pg_refid").asObject().get("Page") != null){

			log.info("IN HERE IN HERE" + jsonObject.get("controllerData").asObject().get("wsc_components_shipment_container_pack_ContainerPackProductListBehaviorController").asObject().get("containerPack_getShipmentLineList_pg_refid").asObject().get("Page").asObject().get("TotalNumberOfPages"));
			if((jsonObject.get("controllerData").asObject().get("wsc_components_shipment_container_pack_ContainerPackProductListBehaviorController").asObject().get("containerPack_getShipmentLineList_pg_refid").asObject().get("Page").asObject().get("TotalNumberOfPages").asString().equals("-1"))){
				
				log.info("got here + in hereeeeee");
				
				pickShipmentNode = jsonObject.get("controllerData").asObject().get("wsc_components_shipment_container_pack_ContainerPackProductListBehaviorController").asObject().get("containerPack_getShipmentLineList_pg_refid").asObject().get("Page").asObject();
				
				isValidPage =pickShipmentNode.get("IsValidPage").asString();
				log.info("got here" + isValidPage);

				if(isValidPage.equals("N")){
					controllerName="perform_pack_controller";
					vars.put("controllerName",controllerName);

					return "OK";

				}if((isValidPage.equals("Y"))){
					controllerName="perform_pack_controller";
					vars.put("controllerName",controllerName);
					pickShipmentArray = jsonObject.get("controllerData").asObject().get("wsc_components_shipment_container_pack_ContainerPackProductListBehaviorController").asObject().get("containerPack_getShipmentLineList_pg_refid").asObject().get("Page").asObject().get("Output").asObject().get("ShipmentLines").asObject().get("ShipmentLine").asArray();
					
					
					
				}else{//the one for where next button is shown although there is none
					controllerName="home_controller";
					
				vars.put("controllerName","home_controller");
				Map lastOrderNumberMap = (Map) (vars.getObject("failedOrderNumbers"));
				if(lastOrderNumberMap == null){
					lastOrderNumberMap = new HashMap();
				}
				lastOrderNumberMap.put(vars.get("currentOrderNumber"), vars.get("currentOrderNumber"));

				vars.putObject("failedOrderNumbers",lastOrderNumberMap);
				log.info("valid page if");

				}

			}else if (jsonObject.get("controllerData").asObject().get("wsc_components_shipment_container_pack_ContainerPackProductListBehaviorController").asObject().get("containerPack_getShipmentLineList_pg_refid").asObject().get("Page").asObject().get("Output").asObject().get("ShipmentLines").asObject().get("ShipmentLine") == null){
				// if(jsonObject.get("controllerData").asObject().get("wsc_components_shipment_container_pack_ContainerPackProductListBehaviorController").asObject().get("containerPack_getShipmentLineList_pg_refid").asObject().get("Page").asObject().get("Output").asObject().get("ShipmentLines").asObject().isEmpty()) {
				log.info("IN All Packed");

				allPacked = true;

				//  }
				allPacked = true;

			} else if(!(jsonObject.get("controllerData").asObject().get("wsc_components_shipment_container_pack_ContainerPackProductListBehaviorController").asObject().get("containerPack_getShipmentLineList_pg_refid").asObject().get("Page").asObject().get("TotalNumberOfPages").equals("-1"))){

				log.info("IN HERE NOW IN");


				String BackroomPickedQuantity;
				pickShipmentNode = jsonObject.get("controllerData").asObject().get("wsc_components_shipment_container_pack_ContainerPackProductListBehaviorController").asObject().get("containerPack_getShipmentLineList_pg_refid").asObject().get("Page").asObject();
				isValidPage =pickShipmentNode.get("IsValidPage").asString();

				if(isValidPage.equals("N")){
					controllerName="home_controller";
					vars.put("controllerName","home_controller");
					Map lastOrderNumberMap = (Map) (vars.getObject("failedOrderNumbers"));
					if(lastOrderNumberMap == null){
						lastOrderNumberMap = new HashMap();
					}
					lastOrderNumberMap.put(vars.get("currentOrderNumber"), vars.get("currentOrderNumber"));

					vars.putObject("failedOrderNumbers",lastOrderNumberMap);
					log.info("valid page if");

					return "OK";

				}else{
					log.info("In first else");

					pickShipmentArray = jsonObject.get("controllerData").asObject().get("wsc_components_shipment_container_pack_ContainerPackProductListBehaviorController").asObject().get("containerPack_getShipmentLineList_pg_refid").asObject().get("Page").asObject().get("Output").asObject().get("ShipmentLines").asObject().get("ShipmentLine").asArray();

					String isLastPage =pickShipmentNode.get("IsLastPage").asString();
					isValidPage =pickShipmentNode.get("IsValidPage").asString();

					JsonObject lastRecordOnProdPage = pickShipmentNode.get("LastRecord").asObject();
					String totalNumberOfProdPages = pickShipmentNode.get("TotalNumberOfPages").asString();

					//used to move across pages
					vars.put("totalNumberOfProdPages", totalNumberOfProdPages);
					// TODO what happens if total pages = 3 or 5
					if (!totalNumberOfProdPages.equals("1")) {
						String prodPageNumber = pickShipmentNode.get("PageNumber").asString();
						int pageNo = Integer.parseInt(prodPageNumber);
						pageNo = pageNo  + 1;
						vars.put("prodPageNumber", ""+pageNo);
					}
					vars.put("lastRecordOnProdPage", ""+lastRecordOnProdPage);
				}
			} else{//TODO refactor to a method
				log.info("IN HERE NOW IN ERROR");

				Map lastOrderNumberMap = (Map) (vars.getObject("failedOrderNumbers"));
				if(lastOrderNumberMap == null){
					lastOrderNumberMap = new HashMap();
				}else {
					lastOrderNumberMap.put(vars.get("currentOrderNumber"), vars.get("currentOrderNumber"));
				}
				vars.putObject("failedOrderNumbers",lastOrderNumberMap);
				controllerName="home_controller";
				vars.put("controllerName",controllerName);
				return "OK";


			}

		}else if((jsonObject.get("controllerData").asObject().get("wsc_components_shipment_container_pack_ContainerPackProductListBehaviorController").asObject().get("containerPack_getShipmentLineList_pg_refid").asObject().get("Page").asObject().get("TotalNumberOfPages").equals("-1"))){
			log.info("%%%%%%%%%%%%%got here");
			
			pickShipmentNode = jsonObject.get("controllerData").asObject().get("wsc_components_shipment_container_pack_ContainerPackProductListBehaviorController").asObject().get("containerPack_getShipmentLineList_pg_refid").asObject().get("Page").asObject();
			isValidPage =pickShipmentNode.get("IsValidPage").asString();

			if(isValidPage.equals("N")){
				controllerName="perform_pack_controller";
				vars.put("controllerName",controllerName);

				return "OK";

			}
			log.info("IN HERE NOW pack controller");

		}else{
			log.info("IN HERE NOW IN ERROR ELSE");

			Map lastOrderNumberMap = (Map) (vars.getObject("failedOrderNumbers"));
			if(lastOrderNumberMap == null){
				lastOrderNumberMap = new HashMap();
			}else {
				lastOrderNumberMap.put(vars.get("currentOrderNumber"), vars.get("currentOrderNumber"));
			}
			vars.putObject("failedOrderNumbers",lastOrderNumberMap);
			controllerName="home_controller";

			vars.put("controllerName",controllerName);
			return "OK";

		}
		log.info("here");
		int pickCount=0;
		String recallFlag = "";
		String illegalFlag = "";
		Map batchPickMap = null;
		Map itemInfoMap =  new HashMap();
		ArrayList pickList = null;
		String OrderNo = "";
		String isLastProdPage = "";
		int  totalNumberOfPages = 1;
		log.info("here");

		//added
		if (vars.getObject("batchPickMap") != null) {
			batchPickMap = (Map) vars.getObject("batchPickMap");
			//Divya - Added to retain the pciklist
			pickList = (ArrayList)vars.getObject("pickList");
			log.info("here if");

		}else{
			log.info("here else");

			batchPickMap = new HashMap();
			pickList = new ArrayList();
		}


		while (!allPacked && pickCount < pickShipmentArray.size() ){
			log.info("here pickcount is from while loop is" +pickCount);
			JsonObject isRecall = null;
			//            JsonObject isRecall = pickShipmentArray.get(pickCount).asObject().get("OrderLine").asObject().get("ItemDetails").asObject().get("Extn").asObject().get("PTItemLocationAttrsList").asObject();
			isLastProdPage = (pickShipmentNode.get("IsLastPage")).asString();
			JsonObject lastRecordOnProdPage = pickShipmentNode.get("LastRecord").asObject();
			totalNumberOfPages = Integer.parseInt((pickShipmentNode.get("TotalNumberOfPages")).asString());
			String Quantity = (pickShipmentArray.get(pickCount).asObject().get("Quantity")).asString();
			String BackroomPickedQuantity = (pickShipmentArray.get(pickCount).asObject().get("OriginalQuantity")).asString();
			String orderLineKey = (pickShipmentArray.get(pickCount).asObject().get("OrderLineKey")).asString();
			String orderHeaderKey = (pickShipmentArray.get(pickCount).asObject().get("OrderHeaderKey")).asString();
			OrderNo = (pickShipmentArray.get(pickCount).asObject().get("OrderNo")).asString();
			String itemID = (pickShipmentArray.get(pickCount).asObject().get("OrderLine").asObject().get("ItemDetails").asObject().get("ItemID")).asString();
			String itemKey = (pickShipmentArray.get(pickCount).asObject().get("OrderLine").asObject().get("ItemDetails").asObject().get("ItemKey")).asString();

			if (isRecall != null) {
				recallFlag = ((isRecall).get("PTItemLocationAttrs").asArray().get(0).asObject().get("IsRecall")).asString();
				illegalFlag = ((isRecall).get("PTItemLocationAttrs").asArray().get(0).asObject().get("IsIllegalIndicator")).asString();
			}else{
				recallFlag ="";
				illegalFlag ="";
			}
			String shipmentLineKey = (pickShipmentArray.get(pickCount).asObject().get("ShipmentLineKey")).asString();
			String shipmentKey = (pickShipmentArray.get(pickCount).asObject().get("ShipmentKey")).asString();
			
			
			batchPickMap.put("isLastPage",isLastProdPage);
			//batchPickMap.put("noOfItemsInPage",noOfItemsInPage);
			itemInfoMap =  new HashMap();
			itemInfoMap.put("isRecall",recallFlag);
			itemInfoMap.put("isIllegal",illegalFlag);
			itemInfoMap.put("shipKey",shipmentKey);
			itemInfoMap.put("shipmentLineKey",shipmentLineKey);
			itemInfoMap.put("itemID",itemID);
			itemInfoMap.put("itemKey",itemID);
			itemInfoMap.put("packRequest",""+pickShipmentArray.get(pickCount));
			itemInfoMap.put("orderLineKey",""+orderLineKey);
			itemInfoMap.put("orderHeaderKey",orderHeaderKey);
			itemInfoMap.put("orderNo",OrderNo);
			String placedQty = pickShipmentArray.get(pickCount).asObject().get("PlacedQuantity").asString();
			double qtyToPick = 0.0;
			log.info("backroom pick is " + (pickShipmentArray.get(pickCount).asObject().get("OriginalQuantity")).asString());
			if(!BackroomPickedQuantity.trim().equals("")){
				qtyToPick = (double) (Double.parseDouble(BackroomPickedQuantity));
			}
			log.info("here after backroom if");


			itemInfoMap.put("qtyToPick",""+qtyToPick);
			itemInfoMap.put("placedQty",""+placedQty);
			pickList.add(itemInfoMap);
			vars.put("shipKey", shipmentKey);
			vars.put("lastRecordOnProdPage", ""+lastRecordOnProdPage);
			
			log.info("picklist is " + pickList);
			pickCount++;
		}
		log.info("Sixe is &&&&&&&"+pickList.size() +"isLastProdPage" +isLastProdPage +"totalNumberOfPages"+ totalNumberOfPages );
		if (pickList.size() ==0 && !allPacked) {
			controllerName="next_page_controller";
			vars.put("lastOrderNumber", OrderNo);
		} else if (pickList.size() ==0 && allPacked) {
			controllerName="finish_pack";
		} else if ((pickList.size() > 0)&&(isLastProdPage.equals("Y")&&(totalNumberOfPages >0))) {
			vars.putObject("batchPickMap",batchPickMap);
			vars.putObject("pickList",pickList);
			vars.put("numberOfItemsToPick",""+pickList.size());
			vars.put("itemPickCount",""+ 0);
			controllerName="perform_pack_controller";
		}else if ((pickList.size() > 0)&&(isLastProdPage.equals("")&&(totalNumberOfPages >0))) {
			vars.putObject("batchPickMap",batchPickMap);
			vars.putObject("pickList",pickList);
			vars.put("numberOfItemsToPick",""+pickList.size());
			vars.put("itemPickCount",""+ 0);
			controllerName="perform_pack_controller";
		}else if ((pickList.size() > 0)&&(isLastProdPage.equals("N")&&(totalNumberOfPages >0))) {
			vars.putObject("batchPickMap",batchPickMap);
			vars.putObject("pickList",pickList);
			vars.put("numberOfItemsToPick",""+pickList.size());
			vars.put("itemPickCount",""+ 0);
			controllerName="perform_pack_controller";
		}else if ((pickList.size() > 0)&&(isLastProdPage.equals("N")&&(totalNumberOfPages < 0))) {
			vars.putObject("batchPickMap",batchPickMap);
			vars.putObject("pickList",pickList);
			vars.put("numberOfItemsToPick",""+pickList.size());
			vars.put("itemPickCount",""+ 0);
			controllerName="next_product_page_controller";
			vars.put("controllerName", controllerName);


		}else {
			// TODO ????
			vars.put("controllerName", "dump_the_world");
			return "OK";

		}

		/*if (controllerName.equals("invalid")) {
			// TODO --- dump lots of data
			vars.put("controllerName", "dump_the world");

		}*/

		vars.put("controllerName", controllerName);
		// TODO not used?  !!!!!!!!!!!!! ??????????????
		//        vars.put("homePageNumber", ""+ homePageNumber);
		return "OK";
	}


}