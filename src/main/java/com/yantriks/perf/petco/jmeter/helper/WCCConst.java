package com.yantriks.perf.petco.jmeter.helper;

/**
 * Created by berniewong on 5/11/17.
 */
public class WCCConst {
//    JmeterVariables Keys
    public static final String VK_CONTROLLER_NAME = "controllerName";
    public static final String VK_REQUEST_NAME = "requestName";
    public static final String VK_RESPONSE_JSON = "responseJson";
    public static final String VK_REQUEST_JSON = "requestJson";    
    
    public static final String VV_ORDER_ERROR_CONTROLLER= "DUMP_THE_WORLD";
    
    public static final String VV_LOGIN_CONTROLLER= "LOGIN_CONTROLlER";
    public static final String VV_LOGIN_REQUEST= "LOGIN_HOME";
    
    public static final String VV_SEARCH_ORDER_CONTROLLER= "SEARCH_ORDER_CONTROLLER";
    public static final String VV_SEARCH_REQUEST_PORTLET ="ORDER_PORTLET"; 
    public static final String VV_SEARCH_REQUEST_WIZARD ="WIZARD"; 
    public static final String VV_SEARCH_REQUEST_EDIT_BEHAVIOUR ="ORDER_EDIT_BEHAVIOUR"; 
    public static final String VV_SEARCH_REQUEST_EDIT_BEHAVIOUR_1 ="ORDER_EDIT_BEHAVIOUR_1"; 
    
   
    
    public static final String VV_VIEW_LINE_SUMMARY_CONTROLLER = "VIEW_LINE_SUMMARY_CONTROLLER";
    public static final String VV_VIEW_LINE_SUMMARY_REQUEST= "VIEW_LINE"; 
    public static final String VV_VIEW_LINE_WIZARD= "WIZARD"; 

    
    public static final String VV_ADD_NOTES_CONTROLLER = "ADD_NOTES_CONTROLLER";
    public static final String VV_ADD_NOTES_REQUEST= "ADD_NOTES";   
    
    public static final String GOTO_ORDERSUMMARY_CONTROLLER = "GOTO_ORDERSUMMARY_CONTROLLER";
    public static final String GOTO_ORDERSUMMARY_REQUEST = "GOTO_SUMM";
    
    public static final String VIEW_SHIPMENTTRACKING_CONTROLLER = "VIEW_SHIPMENTTRACKING_CONTROLLER";
    public static final String VIEW_SHIPMENTTRACKING_REQUEST = "VIEW_SHIP";

}

